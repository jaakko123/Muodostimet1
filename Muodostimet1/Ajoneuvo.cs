﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Muodostimet1
{
    public class Ajoneuvo
    {
       private string tyyppi;
        private string nimi;
        public Ajoneuvo(string tyyppi,string nimi)
        {
            this.tyyppi = tyyppi;
            this.nimi = nimi;
            Console.WriteLine("Ajoneuvo: " + tyyppi +" "+ nimi);
        }
        public string getTyyppi()
        {
            return tyyppi;
        }
        public string getNimi()
        {
            return nimi;
        }
        public override string ToString()
        {
            return tyyppi + " " + nimi;
        }
    }
}
