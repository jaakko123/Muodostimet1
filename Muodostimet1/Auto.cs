﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Muodostimet1
{
    class Auto : Ajoneuvo
    {
        public Auto(string nimi) : base("Auto", nimi)
        {
            Console.WriteLine("Auto " + nimi);
        }
    }
}
